package Base;

import Utils.DriverUtil;
import Utils.DynamicClassObjectUtil;
import Utils.ReadPropertyFileUtil;

import com.experitest.client.Client;
import com.experitest.client.GridClient;
import com.experitest.manager.api.ManagerPublisher;
import com.experitest.manager.client.PManager;
import org.testng.annotations.*;

import java.lang.reflect.Method;
import java.util.Map;


public class Hooks {
    public String osNameGlobal;

    public Client clientHooks;
    public String deviceName;
    public String user;
    public DynamicClassObjectUtil dco;
    public Class classHooks;
    private ManagerPublisher managerPublisher = null;


    @Parameters({"testName", "deviceQuery", "osName", "appPackageName", "appActivity", "iOSApplicationName","username"})
    @BeforeMethod(alwaysRun = true)
    public void setUp(String testName, String deviceQuery, String osName, String appPackage, @Optional("Optional") String appActivity, String iOSApplicationName, Method method,@Optional("Optional") String username) throws Exception {

        System.setProperty("manager.url", "https://foxsports-reporter.experitest.com/");
        dco = new DynamicClassObjectUtil();
        deviceName = deviceQuery;
        osNameGlobal = osName;
        user = username;
        GridClient gridClient = DriverUtil.startDriver(testName + method.getName(), deviceQuery);
        clientHooks = DriverUtil.getClient();
       // ExperitestReportUtil.setClient(clientHooks);
        managerPublisher = gridClient.createPublisher(method.getName(), clientHooks);
        //PManager.setPublisher(managerPublisher);
        if (System.getenv("bamboo_buildNumber") == null) {
            managerPublisher.addProperty("Build", "Local Build");
            managerPublisher.addProperty("Project", "LocalBuild");
        } else {
            managerPublisher.addProperty("Project", System.getenv("bamboo_shortPlanName"));
            managerPublisher.addProperty("BuildNo", "Build:" + System.getenv("bamboo_buildNumber"));


        }

        clientHooks.setReporter("xml", "reports", "Test-" + method.getName() + deviceQuery);
        if (osNameGlobal.toLowerCase().equals("android")) {
            clientHooks.deviceAction("Portrait");
            clientHooks.applicationClearData(appPackage);
            clientHooks.launch(appPackage + "/" + appActivity, true, true);


            //get Android page object class object
            classHooks = dco.getDynamicClass("PageObject.AndroidPageObjects");

        } else {

            clientHooks.deviceAction("Unlock");
            clientHooks.deviceAction("Portrait");
            clientHooks.applicationClearData(iOSApplicationName);
            clientHooks.launch(iOSApplicationName, true, true);


            //get iOS page object class object
            classHooks = dco.getDynamicClass("PageObject.IOSPageObjects");

        }
    }

    @AfterMethod(alwaysRun = true)
    public void tearDownMethodHooks() {
        clientHooks.generateReport(false);
        clientHooks.releaseClient();
    }

}
