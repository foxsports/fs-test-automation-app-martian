package PageAction;

import Utils.DynamicClassObjectUtil;
import com.experitest.client.Client;

public class CommonPageActions {

    Client client;
    Class cls;
    public CommonPageActions(Client clientHooks, Class classHooks) {
        this.client = clientHooks;
        this.cls = classHooks;
    }

    DynamicClassObjectUtil dco = new DynamicClassObjectUtil();


    public boolean elementIsDisplayed(String fieldName) {
        client.waitForElement("NATIVE", dco.getDynamicField(cls,fieldName), 0, 10000);
        boolean found = false;
        found = client.isElementFound("NATIVE",dco.getDynamicField(cls, fieldName));
        return found;
    }

    public boolean elementIsDisplayed2(String fieldName) {
        boolean found = false;
        found = client.isElementFound("NATIVE",dco.getDynamicField(cls, fieldName));

        return found;
    }
    public boolean elementIsNotDisplayed(String fieldName) {
        client.waitForElement("NATIVE", dco.getDynamicField(cls,fieldName), 0, 10000);
        boolean found = false;
        found = !client.isElementFound("NATIVE",dco.getDynamicField(cls, fieldName));
        return found;
    }

    public boolean textElementIsDisplayed(String text) {
        boolean found = false;
        found = client.isElementFound("NATIVE","xpath=//*[@text='"+text+"']");
        return found;
    }

    public boolean textElementIsNotDisplayed(String text) {
        boolean found = false;
        if(!client.isElementFound("NATIVE","xpath=//*[@text='"+text+"']")) {
            found =true;
        }

        return found;
    }

    public void clickText(String text,int index) {

        client.click("NATIVE","xpath=//*[@text='"+text+"']",index,1);
        client.sleep(2000);

    }

    public boolean allElementsAreDisplayed(String fieldName) {
        client.waitForElement("NATIVE", dco.getDynamicField(cls,fieldName), 0, 10000);
        if (client.isElementFound("NATIVE", dco.getDynamicField(cls,fieldName))) {
            int found = client.getElementCount("NATIVE", dco.getDynamicField(cls,fieldName));
            return found > 0;
        }
        return false;
    }

    public boolean givenNoOfElementsAreDisplayed(String fieldName, int elementCount) {
        client.waitForElement("NATIVE", dco.getDynamicField(cls,fieldName), 0, 10000);
        if (client.isElementFound("NATIVE", dco.getDynamicField(cls,fieldName))) {
            int found = client.getElementCount("NATIVE", dco.getDynamicField(cls,fieldName));
            return found >= elementCount;
        }
        return false;
    }



    public boolean clickOnFirstElement(String fieldName,int index) {
        boolean click= false;
        client.waitForElement("NATIVE",dco.getDynamicField(cls, fieldName),0,5000);
        String text = client.elementGetText("NATIVE",dco.getDynamicField(cls, fieldName),index);
        if(!text.isEmpty()){
            client.click("NATIVE",dco.getDynamicField(cls, fieldName),0,1);
            return true;
        }
        return click;
    }

    public boolean elementTextIsDisplayed(String fieldName,int index) {
        boolean found = false;
        client.waitForElement("NATIVE", dco.getDynamicField(cls,fieldName), index, 10000);
        String text = client.elementGetText("NATIVE",dco.getDynamicField(cls, fieldName),index);
        if(!text.isEmpty()){
            found = true;
        }

        return found;
    }

    public void clickElement(String fieldName) {
       // client.waitForElement("NATIVE", dco.getDynamicField(cls, fieldName), 0, 5000);
        client.click("NATIVE", dco.getDynamicField(cls, fieldName), 0, 1);
        client.sleep(2000);
    }





    public void swipeOnElement(int noOfSwipes,String element,String direction) {
        for(int i=0;i<=noOfSwipes;i++) {
            client.elementSwipe("NATIVE", dco.getDynamicField(cls, element), 0, direction, 200, 5000);
        }
    }



    public void waitForElementToShowUp(String fieldName) {
        client.waitForElement("NATIVE", dco.getDynamicField(cls,fieldName), 0, 10000);
        //client.sleep(2000);

    }

    public boolean swipeNotFound(int rounds,String element,String direction,int offset,int time,String deviceName,int offset2) {
        boolean found = false;
        if(deviceName.contains("iPhone X")) {
            found =client.swipeWhileNotFound("Down", offset2, time, "NATIVE", dco.getDynamicField(cls,element), 0, 1000, 10, false);
            return found;
        } else {
            found =client.swipeWhileNotFound("Down", offset, time, "NATIVE", dco.getDynamicField(cls,element), 0, 1000, 10, false);
            return found;
        }
    }

    public void swipeOnElementWIthOffSet(int noOfSwipes,String element,String direction,int offset) {
        for(int i=0;i<=noOfSwipes;i++) {
            client.elementSwipe("NATIVE", dco.getDynamicField(cls, element), 0, direction, offset, 5000);
        }
    }

    public void swipeOnElementWIthAllVals(int noOfSwipes,String direction,int offset,int time,String deviceName,int offset2) {
        if(deviceName.equalsIgnoreCase("iPhone X")) {
            for (int i = 1; i <= noOfSwipes; i++) {
                client.swipe(direction, offset2, time);
            }
        } else {
            for (int i = 1; i <= noOfSwipes; i++) {
                client.swipe(direction, offset, time);
            }
        }
    }




    public void quickSwipe(int noOfSwipes,String direction,int offset,int time) {
        for (int i = 1; i <= noOfSwipes; i++) {
                client.swipe(direction, offset, time);
            }

    }

    public void clickSideNav() {
        client.click("NATIVE",dco.getDynamicField(cls,"sideNav"),0,1);
    }

    public void sendText( String text) {

        client.sendText(text);
    }


}
