package PageAction;

import Base.Hooks;
import Utils.DynamicClassObjectUtil;
import com.experitest.client.Client;
import org.testng.Assert;

public class HomePageActions {

    Client client;
    Class cls;
    public HomePageActions(Client clientHooks, Class classHooks) {
        this.client = clientHooks;
        this.cls = classHooks;
    }

    DynamicClassObjectUtil dco = new DynamicClassObjectUtil();



    public boolean ifCarouselPresent() {
        boolean found = false;
        String carouselId = dco.getDynamicField(cls, "getCarouselId");
        found = client.swipeWhileNotFound("Right", 100, 2000, "NATIVE", carouselId, 1, 400, 5, true);
        return found;
    }

    public void swipeDownToVideoCat() {
        String videoCatId = dco.getDynamicField(cls,"getVideoCatId");
        if(client.swipeWhileNotFound("Down", 50, 2000, "NATIVE", videoCatId, 0, 1000, 5, true)){
        }
    }

    public void clickOnAVideo() {
        String videoToPlayId = dco.getDynamicField(cls,"getVideoCatId");
        client.click("NATIVE", videoToPlayId, 0, 1);
        client.sleep(5000);
    }

    public void clickProfile() {
        client.waitForElement("NATIVE",dco.getDynamicField(cls,"profiles"),0,10000);
        client.click("NATIVE",dco.getDynamicField(cls,"profiles"),0,1);
        client.waitForElementToVanish("NATIVE",dco.getDynamicField(cls,"loadingBtn"),0,10000);
        client.sleep(5000);
    }

    public void clickAllowNotifications() {
        if(client.isElementFound("NATIVE",dco.getDynamicField(cls,"notificationAlert"))) {
            client.click("NATIVE",dco.getDynamicField(cls,"notificationAlert"),0,1);
            client.sleep(5000);
        }

    }


    public void clickSignIN() {
        if(client.isElementFound("NATIVE",dco.getDynamicField(cls,"startSub"))) {
            client.click("NATIVE",dco.getDynamicField(cls,"loginIn"),0,1);
            client.sleep(5000);
        }

    }


    public boolean watchOrStartWatchingISDisplayed() {
        boolean isDisplayed=false;
        if(client.isElementFound("NATIVE",dco.getDynamicField(cls,"heroCarouselWatchVideoBtn"))) {
           isDisplayed=true;
           return isDisplayed;
        } else if(client.isElementFound("NATIVE",dco.getDynamicField(cls,"heroCarouselwatchVideoStart")))  {
            isDisplayed=true;
            return isDisplayed;
        } else {
            return isDisplayed;
        }

    }


    public void clickProfileManage() {
        if(client.isElementFound("NATIVE",dco.getDynamicField(cls, "manage"))) {
            client.click("NATIVE", dco.getDynamicField(cls, "manage"), 0, 1);
        } else {

            client.click("NATIVE",dco.getDynamicField(cls,"profiles"),0,1);
        }
    }

    public void clickDoneBtn() {
        client.click("NATIVE",dco.getDynamicField(cls,"doneBtn"),0,1);
    }
}
