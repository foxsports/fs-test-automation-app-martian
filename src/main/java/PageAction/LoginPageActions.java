package PageAction;

import Utils.DynamicClassObjectUtil;
import Utils.ReadPropertyFileUtil;
import com.experitest.client.Client;
import org.testng.Assert;

public class LoginPageActions {

    private Client client;
    private Class cls;
    DynamicClassObjectUtil abc = new DynamicClassObjectUtil();

    public LoginPageActions(Client clientHooks, Class classHooks) {
        client = clientHooks;
        cls = classHooks;
    }

    ReadPropertyFileUtil rpf = new ReadPropertyFileUtil();
    DynamicClassObjectUtil dco = new DynamicClassObjectUtil();

    public void enterCredentials(String username) {
        String email = dco.getDynamicField(cls,"loginEmailAddress");
        String pwd = dco.getDynamicField(cls,"loginPassword");
        client.elementSendText("NATIVE",email, 0, "");
        if (username != null && !username.isEmpty()) {
            client.elementSendText("NATIVE",email , 0, username);

        } else {
            client.elementSendText("NATIVE",email , 0, rpf.getProperty("user"));
        }

        client.closeKeyboard();
       //client.waitForElement("NATIVE",dco.getDynamicField(cls,"loginPassword"),0,5000);
        client.elementSendText("NATIVE", pwd, 0, "");
        client.elementSendText("NATIVE", pwd, 0, rpf.getProperty("pwd"));

    }

    public void enterCredentialsIos(String username) {
        String email = dco.getDynamicField(cls,"loginEmailAddress");
        String pwd = dco.getDynamicField(cls,"loginPassword");
        if (username != null && !username.isEmpty()) {
            client.elementSendText("NATIVE",email , 0, username);

        } else {
            client.elementSendText("NATIVE",email , 0, rpf.getProperty("user"));
        }
        client.closeKeyboard();
        client.elementSendText("NATIVE", pwd, 0, rpf.getProperty("pwd"));

    }

    public void enterCredentialsSE(String username) {
        System.out.print("***********************"+username);
        String email = dco.getDynamicField(cls,"loginEmailAddressSE");
        String pwd = dco.getDynamicField(cls,"loginPasswordSE");


        if (username != null && !username.isEmpty()) {
            client.elementSendText("NATIVE",email , 0, username);


        } else {
            client.elementSendText("NATIVE",email , 0, rpf.getProperty("user"));
        }

        client.closeKeyboard();
        client.elementSendText("NATIVE", pwd, 0, rpf.getProperty("pwd"));


    }


    public void enterForgotPasswordEmail(String text) {
        client.click("NATIVE", dco.getDynamicField(cls,"forgotPass"), 0,1);
        client.click("NATIVE", dco.getDynamicField(cls,"emailAdd"), 0,1);
        client.sendText(text);
        client.closeKeyboard();
        client.sleep(1000);
        client.click("NATIVE", dco.getDynamicField(cls,"cont"), 0,1);

    }


    public void hitLoginBtn() {
        client.closeKeyboard();
        client.waitForElement("NATIVE",dco.getDynamicField(cls,"loginBtn"),0,5000);
        client.click("NATIVE", dco.getDynamicField(cls,"loginBtn"), 0,1);
        client.sleep(10000);
    }

    public void logOut() {
        client.sleep(2000);
       // client.swipe("Down", 40, 307);
        client.click("NATIVE",dco.getDynamicField(cls,"sideNav"),0,1);
        client.waitForElement("NATIVE",dco.getDynamicField(cls,"myKayoButtons"),0,5000);
        swipeOnElement(2,"myKayoButtons","UP");
        client.click("NATIVE", dco.getDynamicField(cls,"logOutBtn"), 0,1);
        client.sleep(2000);

    }
    public boolean profilePageIsDisplayed() {
        client.waitForElement("NATIVE",dco.getDynamicField(cls,"profiles"),0,5000);
        return client.isElementFound("NATIVE",dco.getDynamicField(cls,"profiles"));

    }
    public void swipeOnElement(int noOfSwipes,String element,String direction) {
        for(int i=0;i<=noOfSwipes;i++) {
//            client.elementSwipe("NATIVE", dco.getDynamicField(cls, element), 1, direction, 400, 5000);
            client.elementSwipe("NATIVE", dco.getDynamicField(cls, element), 1, direction, 300, 5000);
        }
    }


    public void logOutAnd() {
        client.sleep(2000);
        // client.swipe("Down", 40, 307);
        client.waitForElement("NATIVE",dco.getDynamicField(cls,"myKayoButtons"),0,5000);
        swipeOnElement(2,"myKayoButtons","UP");
        client.click("NATIVE", dco.getDynamicField(cls,"logOutBtn"), 0,1);


    }
    public void logOutSE() {
        client.sleep(2000);
        client.swipe("Down", 40, 307);
//        client.click("NATIVE",dco.getDynamicField(cls,"sideNav"),0,1);
        client.waitForElement("NATIVE",dco.getDynamicField(cls,"myKayoButtons"),0,5000);
        swipeOnElement(2,"myKayoButtons","UP");

        client.click("NATIVE", dco.getDynamicField(cls,"logOutBtn"), 0,1);


    }
}
