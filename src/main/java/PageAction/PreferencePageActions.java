package PageAction;

import Utils.DynamicClassObjectUtil;
import com.experitest.client.Client;

public class PreferencePageActions {

    Client client;
    Class cls;
    public PreferencePageActions(Client clientHooks, Class classHooks) {
        this.client = clientHooks;
        this.cls = classHooks;
    }

    DynamicClassObjectUtil dco = new DynamicClassObjectUtil();

    public void clickStartWatching() {
        client.click("NATIVE",dco.getDynamicField(cls,"startWatching"),0,1);
        client.waitForElement("NATIVE",dco.getDynamicField(cls,"homePageLogo"),0,2000);
        client.sleep(3000);
    }


    public void searchAndAddSportsOnHome(String sportToSearch,int index) {
        client.click("NATIVE",dco.getDynamicField(cls,"searchBox"),0,1);
        client.elementSendText("NATIVE",dco.getDynamicField(cls,"searchBox"),0,sportToSearch);
        client.sleep(2000);
        client.click("NATIVE",dco.getDynamicField(cls,sportToSearch),0,1);
        client.closeKeyboard();
        client.click("NATIVE",dco.getDynamicField(cls,"continueBtn"),0,1);
        client.sleep(2000);
        client.click("NATIVE",dco.getDynamicField(cls,"teamToSelect"),index,1);
       // client.click("NATIVE",dco.getDynamicField(cls,sportToSearch),index,1);
        client.click("NATIVE",dco.getDynamicField(cls,"continueBtn"),0,1);
        client.sleep(1000);
        client.click("NATIVE",dco.getDynamicField(cls,"continueBtn"),0,1);
        client.sleep(1000);
        client.click("NATIVE",dco.getDynamicField(cls,"continueBtn"),0,1);
        client.sleep(1000);

    }

    public void searchAndAddSportsCricket(String sportToSearch,int index,int index2) {
        client.click("NATIVE",dco.getDynamicField(cls,"searchBox"),0,1);
        client.elementSendText("NATIVE",dco.getDynamicField(cls,"searchBox"),0,sportToSearch);
        client.sleep(5000);
        client.click("NATIVE",dco.getDynamicField(cls,sportToSearch),index,1);
        client.closeKeyboard();
        client.click("NATIVE",dco.getDynamicField(cls,"continueBtn"),0,1);
        client.sleep(3000);
        client.click("NATIVE",dco.getDynamicField(cls,"followBBl"),1,1);
        client.sleep(1000);
        client.click("NATIVE",dco.getDynamicField(cls,"continueBtn"),0,1);
        client.sleep(1000);


    }


    public void searchAndAddSportsAFL(String sportToSearch,int index,int index2) {
        client.click("NATIVE",dco.getDynamicField(cls,"searchBox"),0,1);
        client.elementSendText("NATIVE",dco.getDynamicField(cls,"searchBox"),0,sportToSearch);
        client.sleep(2000);
        client.click("NATIVE",dco.getDynamicField(cls,"searchItem"),index,1);
        client.closeKeyboard();
        client.click("NATIVE",dco.getDynamicField(cls,"continueBtn"),0,1);
        client.sleep(2000);
        client.click("NATIVE",dco.getDynamicField(cls,"continueBtn"),0,1);
        client.sleep(1000);


    }

}
