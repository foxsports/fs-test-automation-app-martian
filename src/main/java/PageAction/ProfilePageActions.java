package PageAction;

import Utils.DynamicClassObjectUtil;
import com.experitest.client.Client;

public class ProfilePageActions {

    Client client;
    Class cls;
    public ProfilePageActions(Client clientHooks, Class classHooks) {
        this.client = clientHooks;
        this.cls = classHooks;
    }

    DynamicClassObjectUtil dco = new DynamicClassObjectUtil();




    public boolean ifProfilesPresent() {
        boolean found = false;
        String carouselId = dco.getDynamicField(cls, "getCarouselId");
        found = client.swipeWhileNotFound("Right", 100, 2000, "NATIVE", carouselId, 1, 400, 5, true);
        return found;
    }

    public void clickToAddProfile() {
        client.click("NATIVE",dco.getDynamicField(cls,"secondProfile"),0,1);

    }
    public String addProfileDetailsAnd() {
        int random = (int)(Math.random() * 50 + 1);
        String tempProfileName ="Profile"+random;
        client.click("NATIVE",dco.getDynamicField(cls,"enterProfileName"),0,1);
        client.elementSendText("NATIVE",dco.getDynamicField(cls,"enterProfileName"),0,tempProfileName);
        client.closeKeyboard();
        client.click("NATIVE",dco.getDynamicField(cls,"chooseProfileAvatarBlue"),0,1);
        return tempProfileName;
    }

    public void addProfileDetails(String profileText) {
        client.click("NATIVE",dco.getDynamicField(cls,"enterProfileName"),0,1);
        client.elementSendText("NATIVE",dco.getDynamicField(cls,"enterProfileName"),0,profileText);
        client.closeKeyboard();
        client.click("NATIVE",dco.getDynamicField(cls,"chooseProfileAvatarBlue"),0,1);
    }

    public void clickDoneProfile() {
        client.closeKeyboard();
        client.click("NATIVE",dco.getDynamicField(cls,"manageDoneBtn"),0,1);

    }

    public void skipToHome() {
        client.click("NATIVE",dco.getDynamicField(cls,"skipBtn"),0,1);
        client.waitForElement("NATIVE",dco.getDynamicField(cls,"sureSkipPage"),0,5000);
        client.click("NATIVE",dco.getDynamicField(cls,"sureSkipBtn"),0,1);
        client.sleep(3000);
    }


    public void clickProfile2() {
        client.click("NATIVE",dco.getDynamicField(cls,"profileName"),0,1);
        client.sleep(3000);
    }

    public void clickProfile1() {
        client.click("NATIVE",dco.getDynamicField(cls,"profileName1"),0,1);
        client.sleep(3000);
    }
    public void clickDoneBtn() {
        client.click("NATIVE",dco.getDynamicField(cls,"doneBtn"),0,1);
        client.sleep(3000);
    }

    public boolean add() {
        client.waitForElement("NATIVE",dco.getDynamicField(cls,"addProfileText"),0,5000);

        return client.isElementFound("NATIVE",dco.getDynamicField(cls,"addProfileText"));

    }

    public void clickManageBtn() {
        client.click("NATIVE",dco.getDynamicField(cls,"manageDoneBtn"),0,1);
        client.sleep(2000);

    }

    public boolean profile2IsSelected() {
        client.waitForElement("NATIVE",dco.getDynamicField(cls,"blueProfileTick"),0,5000);
        return client.isElementFound("NATIVE",dco.getDynamicField(cls,"blueProfileTick"));

    }

    public boolean addProfilePageIsDisplayed() {
        client.waitForElement("NATIVE",dco.getDynamicField(cls,"addProfileText"),0,5000);
        return client.isElementFound("NATIVE",dco.getDynamicField(cls,"addProfileText"));

    }

    public boolean editProfilePageIsDisplayed() {
        client.waitForElement("NATIVE",dco.getDynamicField(cls,"editProfilePage"),0,5000);
        return client.isElementFound("NATIVE",dco.getDynamicField(cls,"editProfilePage"));

    }

    public void deletePP() {
        client.waitForElement("NATIVE", dco.getDynamicField(cls, "deleteBlueProfile"), 0, 5000);
        client.click("NATIVE",dco.getDynamicField(cls,"deleteBlueProfile"),0,1);

    }

    public void backToPP() {
        client.waitForElement("NATIVE", dco.getDynamicField(cls, "backProfilePage"), 0, 5000);
        client.click("NATIVE",dco.getDynamicField(cls,"backProfilePage"),0,1);

    }

    public boolean profile2IsNotDisplayed() {
        boolean isNotFound = false;
        if(!client.isElementFound("NATIVE",dco.getDynamicField(cls,"profile2OnTop"))) {
             isNotFound = true;
        }

        return isNotFound;

    }


}
