package PageAction;

import Base.Hooks;
import Utils.DriverUtil;
import Utils.DynamicClassObjectUtil;
import Utils.ReadPropertyFileUtil;
import com.experitest.client.Client;


public class VideoControlsPageActions {

    Client client;
    Class cls;
    ReadPropertyFileUtil rpf = new ReadPropertyFileUtil();
    DynamicClassObjectUtil dco = new DynamicClassObjectUtil();

    public VideoControlsPageActions(Client clientHooks, Class classHooks) {
        client = clientHooks;
        cls = classHooks;

    }


    public boolean firstCarouselVideoHasHeaderText(String fieldName, String fieldName2, int index) {
        boolean status= false;
        client.waitForElement("NATIVE", dco.getDynamicField(cls, "firstCarouselVid"), 0, 5000);
        try {
            String text = client.elementGetText("NATIVE", dco.getDynamicField(cls, fieldName), index);
            if (!text.isEmpty()) {
                client.click("NATIVE", dco.getDynamicField(cls, "firstCarouselVid"), 0, 1);
            }

        } catch (Exception e) {

            try {
                String text2 = client.elementGetText("NATIVE", dco.getDynamicField(cls, fieldName2), index);
                if (!text2.isEmpty()) {
                    client.click("NATIVE", dco.getDynamicField(cls, "secondCarouselVid"), 0, 1);
                    status = true;
                }
            } catch (Exception ex) {
                status= false;
            }

        }
        return status;

    }


    public boolean checkCarouselVideoHasHeaders(String fieldName, int index) {
        boolean status= false;
        client.waitForElement("NATIVE", dco.getDynamicField(cls, "firstCarouselVid"), 0, 5000);
        try {
            String text = client.elementGetText("NATIVE", dco.getDynamicField(cls, fieldName), index);
            if (!text.isEmpty()) {
                client.click("NATIVE", dco.getDynamicField(cls, "firstCarouselVid"), 0, 1);
                status=true;
            }

        } catch (Exception e) {

            status= false;

        }
        return status;

    }

    public boolean videoIsPlayed() {
        boolean isVideoPlayed = false;
        String endTime = "";
        client.waitForElement("NATIVE", dco.getDynamicField(cls,"progressBar"), 0, 10);
        // client.swipe("DOWN",200,300);
        client.click("NATIVE", dco.getDynamicField(cls,"videoContainer"),0,1);
        client.click("NATIVE", dco.getDynamicField(cls,"pauseVideo"), 0, 1);
        client.waitForElement("NATIVE", dco.getDynamicField(cls,"timeElapsedVideo"), 0, 10);
        String startTime = client.elementGetText("NATIVE", dco.getDynamicField(cls,"timeElapsedVideo"),0);
        startTime = startTime.replace(":", "");
        client.click("NATIVE", dco.getDynamicField(cls,"videoContainer"),0,1);
        client.click("NATIVE", dco.getDynamicField(cls,"skipFwdVideo"), 0, 1);
        client.waitForElement("NATIVE", dco.getDynamicField(cls,"timeElapsedVideo"), 0, 10);
        client.click("NATIVE", dco.getDynamicField(cls,"skipFwdVideo"), 0, 1);
        client.waitForElement("NATIVE", dco.getDynamicField(cls,"timeElapsedVideo"), 0, 10);
        // client.click("NATIVE", dco.getDynamicField(cls,"playVideo"), 0, 1);
        endTime =client.elementGetText("NATIVE", dco.getDynamicField(cls,"timeElapsedVideo"),0);
        endTime = endTime.replace(":", "");
        isVideoPlayed = Integer.parseInt(endTime) > Integer.parseInt(startTime);
        return isVideoPlayed;

    }
    public boolean videoIsPlayedLiveChannels() {
        boolean isVideoPlayed = false;
        String endTime = "";
        String startTime="";
        //client.sleep(60000);
        client.waitForElement("NATIVE",dco.getDynamicField(cls,"videoContainer"),0,300000);
        client.click("NATIVE", dco.getDynamicField(cls,"videoContainer"),0,1);
        int i = 0;
        while (i < 10) {
            if(client.isElementFound("NATIVE",dco.getDynamicField(cls,"pauseVideo"))){
                client.click("NATIVE", dco.getDynamicField(cls,"pauseVideo"), 0, 1);
                client.click("NATIVE", dco.getDynamicField(cls,"videoContainer"),0,1);
                client.waitForElement("NATIVE", dco.getDynamicField(cls,"timeElapsedVideo"), 0, 10);
                startTime= client.elementGetText("NATIVE", dco.getDynamicField(cls,"timeElapsedVideo"),0);
                client.click("NATIVE", dco.getDynamicField(cls,"videoContainer"),0,1);
                client.click("NATIVE", dco.getDynamicField(cls,"skipBackVideo"), 0, 1);
                client.waitForElement("NATIVE", dco.getDynamicField(cls,"timeElapsedVideo"), 0, 10);
                client.click("NATIVE", dco.getDynamicField(cls,"skipBackVideo"), 0, 1);
                client.waitForElement("NATIVE", dco.getDynamicField(cls,"timeElapsedVideo"), 0, 10);
                client.click("NATIVE", dco.getDynamicField(cls,"skipBackVideo"), 0, 1);
                client.click("NATIVE", dco.getDynamicField(cls,"videoContainer"),0,1);
                client.sleep(60000);
                client.click("NATIVE", dco.getDynamicField(cls,"videoContainer"),0,1);
                endTime =client.elementGetText("NATIVE", dco.getDynamicField(cls,"timeElapsedVideo"),0);
                isVideoPlayed = splitVideoTime(startTime) != splitVideoTime(endTime);
                break;

            } else {
                System.out.println("Try---------- "+i);
                client.sleep(60000);
            }
            i++;
        }

        return isVideoPlayed;

    }

    public int splitVideoTime(String time) {
        String startTime="";
        time = time.replace(":", "");

        String[] times = time.split("/");
        startTime = times[0].trim();

        startTime = startTime.replace("^\"|\"$", "");

        int temp= Integer.parseInt(startTime);

        return temp;
    }
    public void closeVideo(String osName) {
        client.sleep(2000);
        client.click("NATIVE", dco.getDynamicField(cls,"videoContainer"),0,1);
        client.click("NATIVE", dco.getDynamicField(cls,"videoContainer"),0,1);
        client.click("NATIVE", dco.getDynamicField(cls,"backToHomeFromVideo"),0,1);
        client.sleep(5000);
        if(osName.equalsIgnoreCase("Android")) {
            client.swipe("DOWN",200,2000);
        } else {
            client.click("NATIVE", dco.getDynamicField(cls,"homeBtn"),0,1);
        }

        client.sleep(5000);
    }


}
