package PageObject;

public class AndroidPageObjects {
    //************** L O G I N page**************
    public static String loginIn ="xpath=//*[@id='Sign In']";
    public static String startSub ="xpath=//*[@id='Learn More']";
    public static String loginPage ="xpath=//*[@id='login_enter_credentials_root' and @onScreen='true']";
    public static String loginEmailAddress = "//*[@id='login_username_edit_text' and @onScreen='true']";
    public static String loginPassword = "//*[@id='login_password_edit_text' and @onScreen='true']";
    public static String loginBtn = "//*[@id='login_button' and @onScreen='true']";
    public static String doneBtn = "xpath=//*[@text='Done']";
    public static String enterCredsLabel ="xpath=//*[@id='login_enter_credentials_label' and @onScreen='true']";
    public static String createAccountLink ="xpath=//*[@id='login_enter_credentials_create_account_button' and @onScreen='true']";
    public static String loginError ="xpath=//*[@id='login_password_error_message']";
    public static String notificationAlert ="xpath=//*[@text='Allow']";

    //**************Forgot password page**************
    public static String logo = "xpath=//*[@id='kayo_logo']";
    public static String forgotPass = "xpath=//*[@text='Forgot password?']";
    public static String emailAdd ="xpath=//*[@id='password_reset_email_edit_text']";
    public static String signIn ="xpath=//*[@text='Sign into kayo']";
    public static String success ="xpath=//*[@text='Success']";
    public static String emailSuccessMsg ="xpath=//*[@id='snackbar_text']";
    public static String cont="xpath=//*[@text='Continue']";


    // Settimgs page
    public static String settingsListLocator = "class=UINavigationTransitionView";


    //************** H O M E page**************
    public static String homeBtn ="xpath=//*[@id='action_home']";
    public static String loadingBtn = "xpath=//*[@id='item_loading_progress' and @onScreen='true']";
    public static String homePageLogo = "xpath=//*[@id='logo_icon' and @onScreen='true']";
    public static String heroCarousel = "xpath=//*[@id='item_hero_carousel_recycler_view' and @onScreen='true']";
    public static String heroCarouseltitleText = "xpath=//*[@id='item_hero_carousel_item_title_text' and @onScreen='true']";
    public static String heroCarouselwatchVideoStart = "xpath=//*[@id='item_hero_carousel_item_from_start_button' and @onScreen='true']";
    public static String heroCarouselStatusText = "xpath=//*[@id='item_hero_carousel_item_status_text' and @onScreen='true']";
    public static String heroCarouselSynopisisText ="xpath=//*[@id='item_hero_carousel_item_synopsis_text' and @onScreen='true']";
    public static String heroCarouselWatchVideoBtn = "//*[@id='item_hero_carousel_item_from_start_button' and @onScreen='true']";
    public static String sideNav="xpath=//*[@id='top_bar_hamburger_btn']";


    public static String carousel = "xpath=//*[@id='carouselTileItem' and @onScreen='true']";
    public static String carouselLabels = "xpath=//*[@id='item_standard_tile_carousel_text' and @onScreen='true']";
    public static String caroselImage = "xpath=//*[@id=tileMetadataThumbnail and @onScreen='true']";
    public static String liveChannelText ="xpath=//*[@text='Live Channels']";
    public static String mainContainer="xpath=//*[@id='exo_artwork' and @onScreen='true']";
    public static String watchLiveChannel ="xpath=//*[contains(@contentDescription,'Fox')]";
    //public static String watchLiveChannel ="xpath=//*[@text='Live Channels']/following-sibling::android.support.v7.widget.RecyclerView/android.view.ViewGroup[2]";


    // Account settings tab
    public static String accountSettingsTab = "//*[@id='secondary_navigation_tabs' and @onScreen='true']";
    public static String myKayoButtons = "//*[@id='my_kayo_button' and @onScreen='true']";
    public static String logOutBtn = "//*[@text='Log out' and @onScreen='true']";
    public static String logOutInList = "text=Log Out";
    public static String settingCell = "xpath=//*[@id='my_martian_button']";


    public static String getVideoCatId = "xpath=(//*[@id='home_page_recycler_view']/*/*[@id='item_standard_tile_carousel_text'])[4]";

    //************** V I D E O page**************
    public static String firstCarouselVid = "xpath=(//*[@id='item_standard_tile_carousel_video_tile_item_thumbnail' and @onScreen='true'])[2]";
    public static String secondCarouselVid = "xpath=(//*[@id='item_standard_tile_carousel_video_tile_item_thumbnail' and @onScreen='true'])[3]";


    public static String watchVideo = "xpath=//*[@accessibilityLabel='Watch' and @onScreen='true']";
    public static String watchVideoLabels = "xpath=//*[@class='Martian.PosterLabel' and @onScreen='true']";
    public static String getVideoToPlayId = "xpath=(//*[@id='home_page_recycler_view']/*[./*[@id='item_standard_tile_carousel_text']])[4]/*/*[2]";

    public static String firstVideoTextLabel = "xpath=(//*[@id='item_standard_tile_carousel_video_tile_text' and @onScreen='true'])[1]";
    public static String secondVideoTextLabel = "xpath=(//*[@id='item_standard_tile_carousel_video_tile_text' and @onScreen='true'])[2]";


    //play video
    public static String progressBar = "xpath=//*[@id='exo_progress' and @onScreen='true']";
    public static String videoContainer = "xpath=//*[@id='exo_overlay' and @onScreen='true']";
    public static String pauseVideo ="xpath=//*[@id='exo_pause' and @onScreen='true']";
    public static String playVideo ="xpath=//*[@id='exo_play' and @onScreen='true']";
    public static String timeElapsedVideo = "xpath=//*[@id='exo_position' and @onScreen='true']";
    public static String skipFwdVideo = "xpath=//*[@id='exo_skip_forward' and @onScreen='true']";
    public static String skipBackVideo = "xpath=//*[@id='exo_skip_backward' and @onScreen='true']";
    public static String backToHomeFromVideo = "xpath=//*[@id='exo_back_btn' and @onScreen='true']";


    ////************** Profile page**************

    public static String profileLogo = "//*[@id='profile_logo_image' and @onScreen='true']";
    public static String profileHeaderText = "//*[@id='profile_header_text' and @onScreen='true']";
    public static String profiles = "xpath=//*[@id='item_profile_image' and @onScreen='true']";
    public static String manage = "xpath=//*[@id='profile_primary_action_btn' and @onScreen='true']";


    public static String allProfiles ="xpath=//*[@id='item_profile_image']";
    public static String manageDoneBtn ="xpath=//*[@id='profile_primary_action_btn']";
    public static String firstProfile ="xpath=(//*[@id='item_profile_image'])[1]";
    public static String secondProfile="xpath=//*[@contentDescription='Avatar:+']";
    public static String addProfileText="xpath=//*[@id='profile_header_text']";
    public static String createProfilePage="xpath=//*[@id='profile_header_text']";
    public static String enterProfileName ="xpath=//*[@id='profile_name_edit_text']";
    public static String chooseProfileAvatarBlue ="xpath=//*[@contentDescription='Avatar:2']";
    public static String chooseSportPage="xpath=//*[@id='on_boarding_search_content']";
    public static String skipBtn="xpath=//*[@id='on_boarding_skip_action_button']";
    public static String sureSkipPage="xpath=//*[@text='Sure you want to skip?']";
    public static String sureSkipBtn="xpath=//*[@text='Skip' and ./parent::*[@id='on_boarding_skip_confirmation_root']]";
    public static String profileName="xpath=(//*[@id='item_profile_image'])[2]";
    public static String profile2OnTop="xpath=//*[@text='Profile2' and @top='true']";
    public static String profileName1="xpath=//*[@text='Profile1']";
    public static String blueProfileTick="xpath=//*[@text='Profile2' and @id='item_profile_text']";
    public static String deleteBlueProfile="xpath=//*[@id='profile_delete_btn']";
    public static String keyBoardDone="xpath=(//*[@class='UIKBKeyplaneView']/*[@class='UIKBKeyView'])[5]";
    public static String editProfilePage="xpath=//*[@id='profile_header_text']";
    public static String backProfilePage="xpath=//*[@id='logo_icon']";

    ////************** Preferences page**************
    public static String addFav= "xpath=//*[@class='android.widget.ImageButton' and ./parent::*[@id='dock_nav_favourites_recycler_view']]";
    public static String selectedTeams="xpath=//*[@id='dock_nav_favourites_recycler_view']//preceding-sibling::android.widget.ImageButton";
    public static String teamToSelect="xpath=//*[@contentDescription='Heat']";
    public static String nfl="xpath=//*[@contentDescription='NFL']";
    public static String cricket="xpath=//*[@id='item_sport_item' and @contentDescription='cricket']";
    public static String manageFavs="xpath=//*[@text='Manage my teams']";
    public static String yourTeams="xpath=//*[@text='Your teams']";
    public static String yourSports="xpath=//*[@text='Your sports']";
    public static String addSports="xpath=//*[@id='item_sport_item_add']";
    public static String tapToGoBack="xpath=//*[@id='logo_icon' and ./parent::*[@id='app_top_bar']]";
    public static String sportAndTeams="xpath=//*[@id='dock_nav_favourites_recycler_view']//preceding-sibling::android.widget.ImageButton";

    public static String createdPref= "xpath=//*[@accessibilityLabel='NoPref' and @class='Kayo.FSStateButton']";
    public static String searchBox="xpath=//*[@id='on_boarding_search_edit_text']";
    public static String animationView="xpath=//*[@class='LOTAnimationView']";
    public static String continueBtn="xpath=//*[@id='on_boarding_continue_action_button']";
    public static String startWatching="xpath=//*[@text='Start Watching']";
    public static String remove="xpath=//*[@text='Remove']";
    public static String sport1= "xpath=//*[@id='item_sport_item' and @contentDescription='Brisbane Heat']";
    public static String sport2= "xpath=//*[@id='item_sport_item' and @contentDescription='Big Bash League']";
    public static String sport3= "xpath=//*[@id='item_sport_item' and @contentDescription='National Football League']";
    public static String searchItem="xpath=//*[@id='item_sport_item']";
    public static String teamsAddedAfterSearch="xpath=//*[@id='item_sport_item']";
    public static String followBBl="xpath=//*[@text='Follow Big Bash League']";
}
