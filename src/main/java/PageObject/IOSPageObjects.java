package PageObject;

public class IOSPageObjects {
    //************** L O G I N page**************

    public static String loginIn ="xpath=//*[@accessibilityLabel='Sign In' and @class='Kayo.FSStateButton']";
    public static String startSub ="xpath=//*[@accessibilityLabel='Learn More']";
    public static String loginEmailAddress = "xpath=//*[@accessibilityLabel='Enter your email']";
    public static String loginPassword = "xpath=//*[@accessibilityLabel='Your password']";
    public static String loginEmailAddressSE ="//*[@placeholder='Email Address']";
    public static String loginPasswordSE ="//*[@placeholder='Your password']";
    public static String loginBtn = "xpath=//*[@text='Login']";
    public static String okBtn = "xpath=//*[@text='Ok']";
    public static String doneBtn = "xpath=//*[@text='Done']";
    public static String enterCredsLabel ="xpath=//*[@accessibilityLabel='Please sign in' and @onScreen='true']";
    public static String createAccountLink ="xpath=//*[@accessibilityLabel='Create an Account' and @onScreen='true']";
    public static String loginPage ="xpath=//*[@class='UIScrollView' and @onScreen='true']";
    public static String loginError ="xpath=//*[@text=concat('The details you', \"'\", 've entered don', \"'\", 't match what we have.')]";

    //**************Forgot password page**************
    public static String logo= "xpath=//*[@accessibilityIdentifier='transformableLogo' and @onScreen='true']";
    public static String forgotPass = "xpath=//*[@text='Forgot password?']";
    public static String emailAdd ="xpath=//*[@accessibilityLabel='Enter your email']";
    public static String signIn ="xpath=//*[@text='Sign in to Kayo']";
    public static String success ="xpath=//*[@text='Success']";
    public static String cont="xpath=//*[@text='Continue']";
    public static String emailSuccessMsg ="xpath=//*[contains(@text,'Please follow instructions in your email to reset your password')]";


    // Settimgs page
    public static String settingsListLocator = "class=UINavigationTransitionView";
    public static String myKayoButtons = "//*[@accessibilityLabel='My Kayo' and @onScreen='true']";


    //************** H O M E page**************
    public static String homeBtn ="xpath=//*[@text='Home']";
    public static String homePageLogo = "xpath=//*[@accessibilityIdentifier='whiteLogo' and @onScreen='true']";
    public static String loadingBtn = "xpath=//*[@class='UIActivityIndicatorView' and @top='true']";
    public static String heroCarousel = "xpath=//*[@class='Kayo.PosterCollectionViewCell' and @onScreen='true']";
    public static String heroCarouseltitleText = "xpath=(//*[@class='Kayo.PosterLabel'])[2]";
    public static String heroCarouselWatchVideoBtn = "xpath=//*[@accessibilityLabel='Watch' and @onScreen='true']";
    public static String heroCarouselStatusText = "xpath=(//*[@class='Kayo.PosterLabel'])[1]";
    public static String heroCarouselSynopisisText ="xpath=(//*[@class='Kayo.PosterLabel'])[3]";
    public static String notificationAlert ="xpath=//*[@text='Allow']";
    public static String heroCarouselwatchVideoStart = "xpath=//*[@accessibilityLabel='From Start' and @onScreen='true']";
    public static String sideNav="xpath=//*[@accessibilityIdentifier='navHamburgerBtn']";


    public static String carousel = "xpath=//*[@class='Kayo.StandardCarouselCollectionViewCell' and @onScreen='true']";
    public static String carouselLabels = "xpath=//*[@class='FSCore.FSInsetLabel' and @onScreen='true']";
    public static String carouselWatchVideoBtn = "xpath=//*[@class='Kayo.FSStateButton' and @onScreen='true']";
    public static String mainContainer="xpath=//*[@class='Kayo.PosterCollectionViewCell' and @onScreen='true']";
    public static String liveChannelText ="xpath=//*[@text='Live Channels' and @onScreen='true']";
    public static String watchLiveChannel ="xpath=//*[@accessibilityLabel='Live Channels']//..//*[@class='Kayo.StandardCarouselCollectionViewCell' and @onScreen='true' and @hidden='false' and @x<'50']";

    //public static String watchLiveChannel ="xpath=(//*[@accessibilityLabel='Live Channels']//..//*//*[@class='Kayo.StandardCarouselCollectionViewCell'])[3]";

    // Account settings tab
    public static String accountSettingsTab = "xpath=//*[@class='Kayo.TransparentTabBar' and @onScreen='true']";
    public static String myMartianTab = "xpath=//*[@accessibilityLabel='My Kayo' and @onScreen='true']";
    public static String logOutBtn = "//*[@accessibilityLabel='Log out' and @onScreen='true']";
    public static String logOutInList = "text=Log Out";
    public static String settingCell = "xpath=//*[@class='Kayo.MyKayoCollectionViewCell']";


    public static String getVideoCatId = "xpath=(//*[@id='home_page_recycler_view']/*/*[@id='item_standard_tile_carousel_text'])[4]";

    //************** V I D E O page**************
    public static String firstCarouselVid = "xpath=//*[@accessibilityIdentifier='StandardCarouselCollectionViewCell-0:0' and @onScreen='true']";
    public static String secondCarouselVid = "xpath=//*[@accessibilityIdentifier='StandardCarouselCollectionViewCell-0:1' and @onScreen='true']";


    public static String watchVideo = "xpath=//*[@accessibilityLabel='Watch' and @onScreen='true']";
    public static String watchVideoStart = "xpath=//*[@accessibilityLabel='From Start' and @onScreen='true']";
    public static String watchVideoLabels = "xpath=//*[@class='Kayo.PosterLabel' and @onScreen='true']";
    public static String getVideoToPlayId = "xpath=(//*[@id='home_page_recycler_view']/*[./*[@id='item_standard_tile_carousel_text']])[4]/*/*[2]";
    public static String watchBtn ="xpath=//*[@class='Kayo.FSStateButton' and @onScreen='true']";

    public static String firstVideoTextLabel = "xpath=//*[@accessibilityIdentifier='StandardCarouselCollectionViewCell-0:0' and @onScreen='true']/UIView/UIStackView/UILabel";
    public static String secondVideoTextLabel = "xpath=//*[@accessibilityIdentifier='StandardCarouselCollectionViewCell-0:1' and @onScreen='true']/UIView/UIStackView/UILabel";
    public static String backToHome ="xpath=//*[@class='_UIModernBarButton' and ./*[@class='UIImageView']]";

//play video
    public static String progressBar = "xpath=//*[@class='FSPlayer.FSSlider' and @onScreen='true']";
    public static String videoContainer = "xpath=//*[@class='Kayo.MultiviewVideoCellBorderedView']";
    public static String pauseVideo ="xpath=//*[@accessibilityIdentifier='icon_middle_pause' and @onScreen='true']";
   // public static String timeElapsedVideo = "xpath=//*[@class='FSPlayer.FSFunctionalControlsView']/UIStackView/UILabel";
   public static String timeElapsedVideo =  "xpath=(//*[@class='UILabel'])[2]";
    public static String skipFwdVideo = "xpath=//*[@accessibilityIdentifier='icon_middle_skip' and @onScreen='true']";
    public static String backToHomeFromVideo = "xpath=//*[@accessibilityIdentifier='iconNavigationBack' and @onScreen='true']";
    public static String playVideo ="xpath=//*[@accessibilityIdentifier='icon_middle_play' and @onScreen='true']";
    public static String skipBackVideo = "xpath=//*[@accessibilityIdentifier='icon_middle_back' and @onScreen='true']";


    ////************** Profile page**************
    public static String profiles = "xpath=//*[@text='Profile1']";
    public static String manage = "xpath=//*[@text='Manage' and @onScreen='true']";
    public static String allProfiles ="xpath=//*[@class='Kayo.ProfileCollectionViewCell']";
    public static String manageDoneBtn ="xpath=//*[@class='Kayo.FSStateButton']";
    public static String firstProfile ="xpath=//*[@class='Kayo.ProfileCollectionViewCell' and ./*[./*[@accessibilityLabel='Profile1']]]";
    public static String secondProfile="xpath=//*[@class='Kayo.ProfileCollectionViewCell' and ./*[./*[@accessibilityLabel='']]]";
    public static String addProfileText="xpath=//*[@text='Add profile']";
    public static String createProfilePage="xpath=//*[@accessibilityLabel='Create another profile to use on this account']";
    public static String enterProfileName ="xpath=//*[@class='Kayo.UnderlineTextWithValidationView']";
    public static String chooseProfileAvatarBlue ="xpath=(//*[@class='UICollectionView']/*[@class='Kayo.ProfileCollectionViewCell'])[2]";
    public static String chooseSportPage="xpath=//*[@text='All your sport.']";
    public static String skipBtn="xpath=//*[@text and @accessibilityLabel='Skip']";
    public static String sureSkipPage="xpath=//*[@text='Sure you want to skip?']";
    public static String sureSkipBtn="xpath=//*[@text and @accessibilityLabel='Skip' and ./parent::*[./parent::*[@class='UIView']]]";
    public static String profileName="xpath=//*[@text='Profile2' and @onScreen='true']";
    public static String profile2OnTop="xpath=//*[@text='Profile2' and @top='true']";
    public static String profileName1="xpath=//*[@text='Profile1']";
    public static String blueProfileTick="xpath=((//*[@class='UICollectionView']/*/*[@class='UIView' and ./parent::*[@class='Kayo.ProfileCollectionViewCell']])[2]/*[@class='UIImageView' and @width>0 and @height>0])[2]";
    public static String deleteBlueProfile="xpath=//*[@accessibilityLabel='Delete']";
    public static String keyBoardDone="xpath=(//*[@class='UIKBKeyplaneView']/*[@class='UIKBKeyView'])[5]";
    public static String editProfilePage="xpath=//*[@accessibilityLabel='Edit profile']";
    public static String backProfilePage="xpath=//*[@accessibilityLabel='ic24ArrowLEnabled']";


    ////************** Preferences page**************
    public static String addFav= "xpath=//*[@accessibilityIdentifier='AddFavourite']";
    public static String selectedTeams="xpath=//*[@class='Kayo.TeamItemCollectionViewCell']";
    public static String teamToSelect="xpath=//*[@text='Follow Big Bash League']/../../../..//*[@class='Kayo.TeamItemCollectionViewCell'][1]";

    public static String nfl="xpath=//*[@class='Kayo.TeamItemCollectionViewCell']";

    public static String indexOfTeamToSelect="xpath=//*[@class='Kayo.TeamItemCollectionViewCell']";
    public static String manageFavs="xpath=//*[@text='Manage my favourites']";
    public static String yourTeams="xpath=//*[@text='Your teams']";
    public static String yourSports="xpath=//*[@text='Your sports']";
    public static String addSports="xpath=//*[@class='Kayo.OnboardingAddItemCollectionViewCell']";
    public static String tapToGoBack="xpath=//*[@class='Kayo.OnboardingBackControl']";
    public static String sportAndTeams="xpath=//*[@class='Kayo.MenuHeaderCollectionViewCell']";
    public static String createdPref= "xpath=//*[@accessibilityLabel='NoPref' and @class='Kayo.FSStateButton']";
    public static String searchBox="xpath=//*[@class='Kayo.OnboardingSearchBoxView']";
    public static String animationView="xpath=//*[@class='LOTAnimationView']";
    public static String continueBtn="xpath=//*[@accessibilityLabel='Continue']";
    public static String startWatching="xpath=//*[@text='Start Watching']";
    public static String remove="xpath=//*[@accessibilityLabel='Remove']";
    public static String sport1= "xpath=//*[@class='Kayo.TeamItemCollectionViewCell']";
    public static String sport2= "xpath=(//*[@class='UICollectionView' and ./parent::*[@class='UIView'] and (./preceding-sibling::* | ./following-sibling::*)[@class='Kayo.OnboardingTitleHeaderView' and ./*[@text='Your sports']]]/*[@class='Kayo.TeamItemCollectionViewCell'])[1]";
    public static String sport3= "xpath=(//*[@class='UICollectionView' and ./parent::*[@class='UIView'] and (./preceding-sibling::* | ./following-sibling::*)[@class='Kayo.OnboardingTitleHeaderView' and ./*[@text='Your sports']]]/*[@class='Kayo.TeamItemCollectionViewCell'])[1]";

    public static String searchItem="xpath=//*[@class='Kayo.TeamItemCollectionViewCell']";
    public static String cricket="xpath=//*[@class='Kayo.TeamItemCollectionViewCell']";
    public static String teamsAddedAfterSearch="xpath=//*[@class='Kayo.TeamItemCollectionViewCell']";
    public static String followBBl="xpath=//*[@accessibilityLabel='Follow Big Bash League']";



    //video related
    public static String watchCarouselBtn="xpath=//*[@accessibilityLabel='Watch' and @onScreen='true' and @top='true']";
    public static String fromStartBtn="xpath=//*[@accessibilityLabel='From Start' and @onScreen='true' and @top='true']";
}
