package Utils;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Properties;

public class CommonFunctions {


    public static  String constructLoginPayload(String filePath, String email) throws IOException {
        String url = CommonFunctions.class.getResource("").getPath();
        BufferedReader br = new BufferedReader(new InputStreamReader((CommonFunctions.class.getClassLoader().getResourceAsStream(filePath)), StandardCharsets.UTF_8));
        StringBuffer sb = new StringBuffer();
        int num;
        while((num = br.read()) != -1) {
            sb.append((char)num);
        }
        br.close();
        String payload = sb.toString();
        payload = payload.replaceAll("<userEmail>", email);
        return payload;
    }
}
