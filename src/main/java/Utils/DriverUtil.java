package Utils;

import com.experitest.client.Client;
import com.experitest.client.GridClient;

public class DriverUtil {

	public static Client client;
	public static GridClient gridClient;
	private static final ThreadLocal<Client> webDriverLocalThread = new ThreadLocal<Client>();

	private static final Thread CLOSE_THREAD = new Thread() {
		@Override
		public void run() {
			//Do nothing
		}
	};
	static  {
		webDriverLocalThread.remove();
		Runtime.getRuntime().addShutdownHook(CLOSE_THREAD);
	}

	public static GridClient startDriver(String testName, String deviceQuery) {
		ReadPropertyFileUtil rpf = new ReadPropertyFileUtil();
		gridClient = new GridClient( rpf.getProperty("accessKey"), rpf.getProperty("domain"), Integer.valueOf(rpf.getProperty("port")), true  );
		client = gridClient.lockDeviceForExecution(testName, deviceQuery, true,30, 600*2000);
		webDriverLocalThread.set(client);
		return gridClient;
	}

	public static Client getClient(){
		return webDriverLocalThread.get();
	}

}
