package Utils;

public class DynamicClassObjectUtil {

    public Class<?> getDynamicClass(String classname) {
        try {
            Class<?> clazz = Class.forName(classname);
            return clazz;

        } catch (ClassNotFoundException ex) {
            System.err.println(ex + " Object class must be in class path.");
            return null;
        }
    }

    public  String getDynamicField(Class cls, String fieldName)  {
        try {
            try {
                return (cls.getDeclaredField(fieldName).get(null).toString());
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }

        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
        return null;
    }

}
