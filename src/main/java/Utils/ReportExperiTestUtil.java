package Utils;

import com.experitest.client.Client;
import org.testng.Assert;

public class ReportExperiTestUtil {

	Client client;

	public ReportExperiTestUtil(Client clientHooks) {
		this.client = clientHooks;

	}


	public  void assertIfTrue( boolean condition,String errorMessage)  {
        if (condition) {
            Assert.assertTrue( condition,errorMessage);

        } else  {
            client.report(errorMessage,condition);
            Assert.fail(errorMessage);

        }
    }

    public  void reportFail( boolean condition,String errorMessage)  {
	        client.report(errorMessage,condition);
            Assert.fail(errorMessage);


    }

}
