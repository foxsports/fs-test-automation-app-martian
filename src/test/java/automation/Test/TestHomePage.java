package automation.Test;

import Base.Hooks;
import PageAction.CommonPageActions;
import PageAction.HomePageActions;
import PageAction.LoginPageActions;
import Utils.ReportExperiTestUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.lang.reflect.Method;


public class TestHomePage extends Hooks {

    static final Logger log = LoggerFactory.getLogger(TestHomePage.class);

    HomePageActions hp;
    LoginPageActions lp;
    ReportExperiTestUtil rp;
    CommonPageActions cp;

    @BeforeMethod(alwaysRun = true)
    public void beforeEachTest(Method method) {
        log.info("Starting Test ::" + method.getName());
        hp = new HomePageActions(clientHooks, classHooks);
        lp = new LoginPageActions(clientHooks, classHooks);
        rp = new ReportExperiTestUtil(clientHooks);
        cp = new CommonPageActions(clientHooks, classHooks);
        if (osNameGlobal.equalsIgnoreCase("Android")) {
            lp.enterCredentials(user);
            lp.hitLoginBtn();
            hp.clickProfile();
            hp.clickAllowNotifications();
        } else {
            hp.clickSignIN();
            lp.enterCredentialsIos(user);
            lp.hitLoginBtn();
            hp.clickProfile();
            hp.clickAllowNotifications();

        }
    }


    @Test(groups = {"regression","homepage"})
    public void testHomePageHero()  {
        rp.assertIfTrue(cp.elementIsDisplayed("homePageLogo"),"HomePage Logo is not displayed");
        rp.assertIfTrue(cp.elementIsDisplayed("heroCarousel"),"Hero carousel image is not displayed");
        rp.assertIfTrue(hp.watchOrStartWatchingISDisplayed(),"Hero carousel watch video is not displayed");
        rp.assertIfTrue(cp.elementIsDisplayed2("heroCarouseltitleText"),"Hero Carousel Title text is not displayed");
        rp.assertIfTrue(cp.elementIsDisplayed2("heroCarouselSynopisisText"),"Hero Carousel Synopisis text is not displayed");


    }

    @Test(groups = {"regression","homepage"})
    public void testHomePageTopCarousel()  {
        cp.swipeNotFound(5,"carousel","DOWN",300,5000,"ANY",400);
        rp.assertIfTrue(cp.elementIsDisplayed("carousel"),"Carousel image is not displayed");

    }


}
