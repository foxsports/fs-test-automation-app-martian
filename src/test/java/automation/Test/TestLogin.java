package automation.Test;

import Base.Hooks;
import PageAction.CommonPageActions;
import PageAction.HomePageActions;
import PageAction.LoginPageActions;
import Utils.ReportExperiTestUtil;

import org.junit.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.lang.reflect.Method;

public class TestLogin extends Hooks{
    static final Logger log = LoggerFactory.getLogger(TestLogin.class);
    HomePageActions hp ;
    LoginPageActions lp ;
    ReportExperiTestUtil rp;
    CommonPageActions cp;

    @BeforeMethod(alwaysRun = true)
    public void beforeEachTest(Method method) {
        log.info("Starting Test ::" +method.getName());
        hp = new HomePageActions(clientHooks, classHooks);
        lp = new LoginPageActions(clientHooks, classHooks);
        rp = new ReportExperiTestUtil(clientHooks);
        cp = new CommonPageActions(clientHooks, classHooks);
        hp.clickSignIN();
    }

    @Test (groups = { "regression","login"})
    public void testLoginPageElements()  {
        rp.assertIfTrue(cp.elementIsDisplayed("loginEmailAddress"), "Login Email adress text input box is not displayed");
        rp.assertIfTrue(cp.elementIsDisplayed("loginPassword"), "Login Password text input box is not displayed");
        rp.assertIfTrue(cp.elementIsDisplayed("loginBtn"), "Login Button is not displayed");
        rp.assertIfTrue(cp.elementIsDisplayed("enterCredsLabel"),"Please enter credentials label is not displayed");

    }
    @Test (groups = { "regression","login"})
    public void testLoginLogout()  {
        if (osNameGlobal.equalsIgnoreCase("Android")) {
            lp.enterCredentials(user);

        } else {

            lp.enterCredentialsIos(user);

        }

        lp.hitLoginBtn();
        rp.assertIfTrue(lp.profilePageIsDisplayed(),"Profiles page was not displayed after login");
        hp.clickProfile();
        rp.assertIfTrue(cp.elementIsDisplayed("homePageLogo"),"HomePage Logo is not displayed");
        lp.logOut();

    }


    @Test (groups = { "regression","login"})
    public void testLoginIncorrectDetails()  {
        lp.enterCredentials("incorrect");
        lp.hitLoginBtn();
        rp.assertIfTrue(cp.elementIsDisplayed("loginError"),"No login error message is displayed");

    }


    @Test (groups = { "regression","login"})
    public void testForgotPassWordElements()  {
        cp.clickElement("forgotPass");
        rp.assertIfTrue(cp.elementIsDisplayed("logo"), "Kayo homepage logo is not displayed");
        rp.assertIfTrue(cp.elementIsDisplayed("emailAdd"), "Email address text box is not displayed");
        rp.assertIfTrue(cp.elementIsDisplayed("signIn"), "SignIn to Kayo link is not displayed");
        rp.assertIfTrue(cp.elementIsDisplayed("forgotPass"),"Forgot password text is not displayed");

    }

    @Test (groups = { "regression","iosTest","login"})
    public void testForgotPassWordEmailMessage()  {
        lp.enterForgotPasswordEmail("temp@temp.com");
        rp.assertIfTrue(cp.elementIsDisplayed2("emailSuccessMsg"), "Email is sent message is not displayed");

    }



}
