package automation.Test;

import Base.Hooks;
import PageAction.*;
import Utils.ReportExperiTestUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


import java.lang.reflect.Method;

public class TestPreferencePage extends Hooks{
    PreferencePageActions pref ;
    static final Logger log = LoggerFactory.getLogger(TestPreferencePage.class);
    LoginPageActions lp;
    HomePageActions hp;
    ProfilePageActions pp;
    ReportExperiTestUtil rp;
    CommonPageActions cp;
    @BeforeMethod(alwaysRun = true)
    public void beforeEachTest(Method method) {
        log.info("Starting Test ::" + method.getName());
        pref = new PreferencePageActions(clientHooks, classHooks);

        hp = new HomePageActions(clientHooks, classHooks);
        lp = new LoginPageActions(clientHooks, classHooks);
        rp = new ReportExperiTestUtil(clientHooks);
        cp= new CommonPageActions(clientHooks,classHooks);
        pp= new ProfilePageActions(clientHooks,classHooks);

        if (osNameGlobal.equalsIgnoreCase("Android")) {
            lp.enterCredentials(user);
            lp.hitLoginBtn();
            pp.clickToAddProfile();
            pp.addProfileDetails("NoPref");
            pp.clickDoneBtn();

        } else {
            hp.clickSignIN();
            if(deviceName.contains("iPhone SE")) {
                lp.enterCredentialsSE(user);
            } else {
                lp.enterCredentialsIos(user);
            }

            lp.hitLoginBtn();
            pp.clickToAddProfile();
            pp.addProfileDetails("NoPref");
            pp.clickDoneBtn();


        }
    }

    @Test (groups = {"pref","regression","notSE"})
    public void noPrefNewProfile()  {
        pp.skipToHome();
        cp.clickSideNav();
        rp.assertIfTrue(cp.elementIsDisplayed("addFav"),"Add Fav button is not displayed");
        rp.assertIfTrue(cp.elementIsNotDisplayed("selectedTeams"),"Fav teams are displayed");
        cp.clickElement("manageFavs");
        rp.assertIfTrue(cp.givenNoOfElementsAreDisplayed("addSports",2),"Fav teams are displayed");
        cp.clickElement("tapToGoBack");

    }



    @Test (groups = { "pref","regression","notSE"})
    public void addPrefFromNewProfile()  {
        pref.searchAndAddSportsOnHome("nfl",0);
        rp.assertIfTrue(cp.givenNoOfElementsAreDisplayed("teamsAddedAfterSearch",3),"Fav teams are not displayed");
        pref.clickStartWatching();
        cp.clickSideNav();
        rp.assertIfTrue(cp.elementIsDisplayed("addFav"),"Add Fav button is not displayed");
        rp.assertIfTrue(cp.givenNoOfElementsAreDisplayed("sportAndTeams",3),"No Fav teams are displayed in side nav");
        cp.clickElement("manageFavs");
        rp.assertIfTrue(cp.givenNoOfElementsAreDisplayed("selectedTeams",3),"Fav teams are not displayed in manage favs");
        cp.clickElement("tapToGoBack");

    }




    @Test (groups = { "pref","regression","notSE","iosTest"})
    public void addPrefNewProfileFromSideNav()  {
        pp.skipToHome();
        cp.clickSideNav();
        rp.assertIfTrue(cp.elementIsDisplayed("addFav"),"Add Fav button is not displayed");
        rp.assertIfTrue(cp.elementIsNotDisplayed("selectedTeams"),"Fav teams are displayed");
        cp.clickElement("addFav");
        pref.searchAndAddSportsCricket("cricket",1,2);
        rp.assertIfTrue(cp.givenNoOfElementsAreDisplayed("sportAndTeams",3),"Fav teams are displayed");

    }


//    @Test (groups = { "pref","regression","notSE","ab"})
//    public void addPrefNewProfileFromManageFavs()  {
//        pp.skipToHome();
//        cp.clickSideNav();
//        rp.assertIfTrue(cp.elementIsDisplayed("addFav"),"Add Fav button is  displayed","Add Fav button is not displayed");
//        rp.assertIfTrue(cp.elementIsNotDisplayed("selectedTeams"),"No fav teams are displayed","Fav teams are displayed");
//        cp.clickElement("manageFavs");
//        pref.searchAndAddSportsAFL("brisbane lions",1,2);
//        rp.assertIfTrue(cp.givenNoOfElementsAreDisplayed("selectedTeams",4),"3 fav teams are displayed in Manage favs","Fav teams are not displayed in manage favs");
//        cp.clickElement("tapToGoBack");
//        rp.assertIfTrue(cp.givenNoOfElementsAreDisplayed("sportAndTeams",3),"No fav teams are displayed","Fav teams are displayed");
//
//    }



    @Test (groups = { "pref","regression","notSE"})
    public void addDeleteProfile()  {
        pref.searchAndAddSportsOnHome("nfl",0);
        rp.assertIfTrue(cp.givenNoOfElementsAreDisplayed("teamsAddedAfterSearch",3),"Fav teams are not displayed");
        pref.clickStartWatching();
        cp.clickSideNav();
        rp.assertIfTrue(cp.givenNoOfElementsAreDisplayed("sportAndTeams",3),"No Fav teams are displayed in side nav");
        cp.clickElement("manageFavs");
        cp.clickElement("sport1");
        cp.clickElement("remove");
        cp.clickElement("sport2");
        cp.clickElement("remove");
        cp.clickElement("sport3");
        cp.clickElement("remove");
        cp.clickElement("tapToGoBack");
        rp.assertIfTrue(cp.elementIsNotDisplayed("selectedTeams"),"Fav teams are displayed");
        cp.clickElement("manageFavs");
        rp.assertIfTrue(cp.elementIsNotDisplayed("selectedTeams"),"Fav teams are displayed");
        cp.clickElement("tapToGoBack");
    }

    @AfterMethod(alwaysRun = true)
    public void tearDownMethod(Method method) {
        cp.clickText("NoPref", 0);
        pp.clickManageBtn();
        if (osNameGlobal.equalsIgnoreCase("Android")) {
            cp.clickText("NoPref", 1);
        } else {
            cp.clickText("NoPref", 0);
        }
        pp.deletePP();
        pp.backToPP();
       // pp.clickProfile1();

        log.info("After Test ::" + method.getName() + "finish");
    }
}
