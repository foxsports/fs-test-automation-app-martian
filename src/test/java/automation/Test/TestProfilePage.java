package automation.Test;

import Base.Hooks;
import PageAction.*;
import Utils.ReportExperiTestUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.lang.reflect.Method;

public class TestProfilePage extends Hooks{
    VideoControlsPageActions vc = new VideoControlsPageActions(clientHooks, classHooks);
    static final Logger log = LoggerFactory.getLogger(TestProfilePage.class);
    LoginPageActions lp;
    HomePageActions hp;
    ProfilePageActions pp;
    ReportExperiTestUtil rp;
    CommonPageActions cp;

    @BeforeMethod(alwaysRun = true)
    public void beforeEachTest(Method method) {
        log.info("Starting Test ::" +method.getName());


        hp = new HomePageActions(clientHooks, classHooks);
        lp = new LoginPageActions(clientHooks, classHooks);
        rp = new ReportExperiTestUtil(clientHooks);
        cp= new CommonPageActions(clientHooks,classHooks);
        pp= new ProfilePageActions(clientHooks,classHooks);
       
        if (osNameGlobal.equalsIgnoreCase("Android")) {
            lp.enterCredentials(user);
            lp.hitLoginBtn();

        } else {
            hp.clickSignIN();
            if(deviceName.contains("iPhone SE")) {
                lp.enterCredentialsSE(user);
            } else {
                lp.enterCredentialsIos(user);
            }

            lp.hitLoginBtn();

        }
    }




    @Test (groups = { "iosTest","profileIos","regression"})
    public void addDeleteProfies()  {
        rp.assertIfTrue(lp.profilePageIsDisplayed(),"Profiles page was not displayed after login");
        pp.clickToAddProfile();
        rp.assertIfTrue(pp.addProfilePageIsDisplayed(),"Add Profiles page was not displayed");
        pp.addProfileDetails("Profile2");
        pp.clickDoneBtn();
        rp.assertIfTrue(cp.elementIsDisplayed("chooseSportPage"),"Choose Sport page is not displayed");
        pp.skipToHome();
        rp.assertIfTrue(cp.elementIsDisplayed("homePageLogo"),"HomePage Logo is not displayed");
        rp.assertIfTrue(cp.elementIsDisplayed("heroCarousel"),"Hero carousel image is not displayed");
        cp.clickSideNav();
        rp.assertIfTrue(cp.elementIsDisplayed("profileName"),"Profile you just created is not displayed");
        pp.clickProfile2();
        rp.assertIfTrue(lp.profilePageIsDisplayed(),"Profiles page was not displayed after login");
        rp.assertIfTrue(pp.profile2IsSelected(),"Profile 2 is not selected");
        pp.clickManageBtn();
        pp.clickProfile2();
        rp.assertIfTrue(pp.editProfilePageIsDisplayed(),"Profile page is not displayed");

        pp.deletePP();
        pp.backToPP();
        rp.assertIfTrue(pp.profile2IsNotDisplayed(),"Profile 2 is not deleted succesfully");
        pp.clickProfile1();

    }

    @Test (groups = { "profileAnd","andTest","regression"})
    public void addDeleteProfiesAnd()  {
        String profileName="";
        rp.assertIfTrue(lp.profilePageIsDisplayed(),"Profiles page was not displayed after login");
        pp.clickToAddProfile();
        rp.assertIfTrue(pp.addProfilePageIsDisplayed(),"Add Profiles page was not displayed");
        profileName= pp.addProfileDetailsAnd();
        pp.clickDoneBtn();
        rp.assertIfTrue(cp.elementIsDisplayed("chooseSportPage"),"Choose Sport page is not displayed");
        pp.skipToHome();
        rp.assertIfTrue(cp.elementIsDisplayed("homePageLogo"),"HomePage Logo is not displayed");
        rp.assertIfTrue(cp.elementIsDisplayed("heroCarousel"),"Hero carousel image is not displayed");
        cp.clickSideNav();
        rp.assertIfTrue(cp.textElementIsDisplayed(profileName),"Profile you just created is not displayed");
        cp.clickText(profileName,0);
        rp.assertIfTrue(lp.profilePageIsDisplayed(),"Profiles page was not displayed after login");
        rp.assertIfTrue(cp.textElementIsDisplayed(profileName),"Profile 2 is not selected");
        pp.clickManageBtn();
        rp.assertIfTrue(pp.editProfilePageIsDisplayed(),"Profile page is not displayed");
        cp.clickText(profileName,1);
        pp.deletePP();
        rp.assertIfTrue(cp.textElementIsNotDisplayed(profileName),"Profile 2 is not deleted succesfully");
        pp.clickProfile1();

    }

}
