package automation.Test;

import Base.Hooks;
import PageAction.CommonPageActions;
import PageAction.HomePageActions;
import PageAction.LoginPageActions;
import PageAction.VideoControlsPageActions;
import Utils.ReportExperiTestUtil;

import org.boon.core.Sys;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.lang.reflect.Method;

public class TestVideoControls extends Hooks{
    VideoControlsPageActions vc = new VideoControlsPageActions(clientHooks, classHooks);
    static final Logger log = LoggerFactory.getLogger(TestVideoControls.class);
    LoginPageActions lp;
    HomePageActions hp;
    VideoControlsPageActions vp;
    ReportExperiTestUtil rp;
    CommonPageActions cp;

    @BeforeMethod(alwaysRun = true)
    public void beforeEachTest(Method method) {
        log.info("Starting Test ::" +method.getName());
        hp = new HomePageActions(clientHooks, classHooks);
        lp = new LoginPageActions(clientHooks, classHooks);
        rp = new ReportExperiTestUtil(clientHooks);
        cp= new CommonPageActions(clientHooks,classHooks);
        vp= new VideoControlsPageActions(clientHooks,classHooks);
        if (osNameGlobal.equalsIgnoreCase("Android")) {
            lp.enterCredentials(user);
            lp.hitLoginBtn();
            hp.clickProfile();
            hp.clickAllowNotifications();
        } else {
            hp.clickSignIN();
            lp.enterCredentialsIos(user);
            lp.hitLoginBtn();
            hp.clickProfile();
            hp.clickAllowNotifications();
        }
    }


//    @Test (groups = { "xyz","iosTest" })
//    public void videoPlays()  {
//        cp.waitForElementToShowUp("watchBtn");
//        cp.clickOnFirstElement("watchBtn",0);
//        cp.waitForElementToShowUp("watchBtn");
//        cp.clickOnFirstElement("watchBtn",0);
//        rp.assertIfTrue(vp.videoIsPlayed(), "","Video doesnt play");
//        //hpActions.closeVideo();
//
//    }

    @Test (groups = { "regression","galaxyTab","video","noVideo"})
    public void videoPlaysLiveChannels()  {

        cp.swipeNotFound(10,"watchLiveChannel","DOWN",300,5000,deviceName,400);

        if(deviceName.contains("Google") || deviceName.contains("S8") ) {
            cp.quickSwipe(1,"DOWN",300,5000);
        } else if(deviceName.contains("7+")) {
            cp.quickSwipe(1,"DOWN",450,5000);
        }
        cp.waitForElementToShowUp("liveChannelText");
        cp.clickElement("watchLiveChannel");
        rp.assertIfTrue(vp.videoIsPlayedLiveChannels(), "Video doesnt play");
        vp.closeVideo(osNameGlobal);


    }

//
//    @Test (groups = { "b" })
//    public void testVideoWatchPg()  {
//
//        if(vp.checkCarouselVideoHasHeaders("firstVideoTextLabel",0)) {
//            rp.assertIfTrue(cp.elementIsDisplayed("watchVideo"),"","Watch video is not displayed");
//            rp.assertIfTrue(cp.elementTextIsDisplayed("watchVideoLabels",0),"","Match Info header is not displayed");
//            rp.assertIfTrue(cp.elementTextIsDisplayed("watchVideoLabels",1),"","Match game info is not displayed");
//            cp.clickElement("backToHome");
//        } else if(vp.checkCarouselVideoHasHeaders("secondVideoTextLabel",0)) {
//            rp.assertIfTrue(cp.elementIsDisplayed("watchVideo"),"","Watch video is not displayed");
//            rp.assertIfTrue(cp.elementTextIsDisplayed("watchVideoLabels",0),"","Match Info header is not displayed");
//            rp.assertIfTrue(cp.elementTextIsDisplayed("watchVideoLabels",1),"","Match game info is not displayed");
//            cp.clickElement("backToHome");
//        } else {
//            log.info("The Video with text headers is not present so nothing to test");
//        }
//
//        }


}
